//
//  ImageViewController.swift
//  ImageSearch
//
//  Created by Владислав on 25.11.2020.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet private weak var collectionView: UICollectionView!
    private var activityIndicator = UIActivityIndicatorView()
    
    private var images: [UIImage?] = []
    private var imagesInfo = [ImageInfo]()
    
    private let spacing: CGFloat = 5
    private let numberOfItemsPerRow: CGFloat = 3
    
    //MARK:- Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        //loadImages()
        getCachedImages()
    }
    
    private func configure() {
        collectionView.delegate = self
        collectionView.dataSource = self
        setupSpinner()
        setupSearchController()
    }
    
    private func setupSearchController() {
        let searchC = UISearchController(searchResultsController: nil)
        searchC.searchBar.placeholder = "Search"
        //searchC.searchResultsUpdater = self
        searchC.searchBar.delegate = self
        
        //Forgot about this line during the class. We should add it because we don't use searchResultsController
        searchC.obscuresBackgroundDuringPresentation = false
        
        navigationItem.searchController = searchC
    }
    
    private func setupSpinner() {
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        activityIndicator.color = .red
        activityIndicator.frame = collectionView.bounds
        activityIndicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.addSubview(activityIndicator)
    }
    
    private func loadImages(query: String) {
        images.removeAll()
        updateUI()
        activityIndicator.startAnimating()
        NetworkService.shared.fetchImages(query: query, amount: 50) { (result) in
            self.activityIndicator.stopAnimating()
            switch result {
            case let .failure(error):
                print(error)
                
            case let .success(imagesInfo):
                self.imagesInfo = imagesInfo
                self.images = Array(repeating: nil, count: imagesInfo.count)
                self.updateUI()
            }
        }
    }
    
    private func updateUI() {
        self.collectionView.reloadSections(IndexSet(arrayLiteral: 0))
    }

    private func getCachedImages() {
        CacheManager.shared.getCachedImages { (images) in
            self.images = images
            self.updateUI()
        }
    }
    
    private func loadImage(for cell: ImageCell, at index: Int) {
        if let image = images[index] {
            cell.configure(with: image)
            return
        }
        let info = imagesInfo[index]
        NetworkService.shared.loadImage(from: info.webformatURL) { (image) in
            if index < self.images.count {
                self.images[index] = image
                CacheManager.shared.cacheImage(image, with: info.id)
                cell.configure(with: self.images[index])
            }
        }
    }

}

//MARK:- Data Source & Delegate
extension ImageViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.identifier, for: indexPath) as? ImageCell else {
            fatalError("Invalid Cell Kind")
        }
//        cell.layer.borderColor = UIColor.black.cgColor
//        cell.layer.borderWidth = 2

        loadImage(for: cell, at: indexPath.row)
        
        return cell
    }
}


//MARK:- Flow Layout
extension ImageViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.bounds.width
        let summarySpacing = spacing * (numberOfItemsPerRow - 1)
        let insets = 2 * spacing  // leftInset + rightInset (of section)
        
        let rawWidth = width - summarySpacing - insets
        
        let cellWidth = rawWidth / numberOfItemsPerRow
        
        return CGSize(width: cellWidth, height: cellWidth)
        
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: spacing,
                     left: spacing,
                     bottom: spacing,
                     right: spacing
        )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
}


extension ImageViewController : UISearchBarDelegate {
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, query.count >= 3 else {
            return
        }
        loadImages(query: query)
    }
}

//extension ImageViewController : UISearchResultsUpdating {
//    func updateSearchResults(for searchController: UISearchController) {
//        guard let query = searchController.searchBar.text, query.count >= 3 else {
//            return
//        }
//        loadImages(query: query)
//    }
//
//}
