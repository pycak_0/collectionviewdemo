//
//  SessionError.swift
//  ImageSearch
//
//  Created by Владислав on 02.12.2020.
//

import Foundation

enum SessionError: Error {
    case invalidUrl, decodingError(Error), serverError(_ statusCode: Int), other(Error)
    
    var localizedDescription: String {
        switch self {
        case .invalidUrl:
            return "Некорректный URL-адрес"
        case let .decodingError(error):
            return error.localizedDescription
        case let .serverError(statusCode):
            return "Не удалось связаться с сервером (\(statusCode))"
        case let .other(error):
            return error.localizedDescription
        }
    }
    
}

//let error = SessionError.serverError(404)
//error.localizedDescription


//
//
//enum PaymentType: String {
//    case cash = "sfsfsdfd"
//    case applePay
//    case bankCard
//
//
//}
//
//
//let type  = PaymentType.bankCard
//
//func foo() {
//    switch type {
//    case .applePay:
//        break
//    case .bankCard:
//        break
//    case .cash:
//        break
//    }
//
//}
//
