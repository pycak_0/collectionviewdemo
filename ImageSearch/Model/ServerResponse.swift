//
//  ServerResponse.swift
//  ImageSearch
//
//  Created by Владислав on 02.12.2020.
//

import Foundation

struct ServerResponse<Object: Decodable>: Decodable {
    var hits: [Object]
}
