//
//  ImageInfo.swift
//  ImageSearch
//
//  Created by Владислав on 02.12.2020.
//

import Foundation

struct ImageInfo: Decodable {
    var id: Int
    var previewURL: URL?
    var webformatURL: URL?
    
}
